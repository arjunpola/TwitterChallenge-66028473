package challenge.twitter.com.twitterchallenge.mvp.views;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import challenge.twitter.com.twitterchallenge.mvp.model.WeatherData;

/**
 * Created by arjunpola on 5/26/17.
 */

public interface WeatherView extends MvpView {

    void showCurrentWeatherInfo(WeatherData weatherData);

    void showStdDeviation(String stdTempCel, String stdTempFah);
}
