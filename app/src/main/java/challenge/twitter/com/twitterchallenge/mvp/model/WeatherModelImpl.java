package challenge.twitter.com.twitterchallenge.mvp.model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import challenge.twitter.com.twitterchallenge.network.WeatherAPI;

/**
 * Created by arjunpola on 5/26/17.
 */

public class WeatherModelImpl implements WeatherModel {

    WeatherAPI weatherAPI;
    private static final String BASE_URL = "http://twitter-code-challenge.s3-website-us-east-1.amazonaws.com/";

    public WeatherModelImpl() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        weatherAPI = retrofit.create(WeatherAPI.class);
    }

    @Override
    public void getCurrentWeather() {
        weatherAPI.fetchWeather(getWeatherParam(0)).enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                EventBus.getDefault().post(new WeatherInfoEvent(response.body()));
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                Log.e("Current Weather", t.getMessage());
            }
        });
    }

    @Override
    public void getWeatherForNext5Days() {
        final List<WeatherData> weatherData = Collections.synchronizedList(new ArrayList<WeatherData>());
        for (int i = 1; i <= 5; i++) {
            final int callIndex = i;
            weatherAPI.fetchWeather(getWeatherParam(i)).enqueue(new Callback<WeatherData>() {
                @Override
                public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                    weatherData.add(response.body());
                    if (weatherData.size() == 5) {
                        EventBus.getDefault().post(new WeatherInfoListEvent(weatherData));
                    }
                }

                @Override
                public void onFailure(Call<WeatherData> call, Throwable t) {
                    Log.e("Std Weather " + callIndex, t.getMessage());
                }
            });
        }
    }

    String getWeatherParam(int i) {
        if (i == 0)
            return "current.json";
        else
            return "future_" + i + ".json";
    }


    public static final class WeatherInfoEvent {
        private WeatherData weatherData;

        WeatherInfoEvent(WeatherData weatherData) {
            this.weatherData = weatherData;
        }

        public WeatherData getWeatherData() {
            return weatherData;
        }
    }

    public static final class WeatherInfoListEvent {
        private List<WeatherData> weatherDataList;

        public WeatherInfoListEvent(List<WeatherData> weatherDataList) {
            this.weatherDataList = weatherDataList;
        }

        public List<WeatherData> getWeatherDataList() {
            return weatherDataList;
        }
    }
}
