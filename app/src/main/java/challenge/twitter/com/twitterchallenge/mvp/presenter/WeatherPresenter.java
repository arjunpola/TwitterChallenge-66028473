package challenge.twitter.com.twitterchallenge.mvp.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import challenge.twitter.com.twitterchallenge.mvp.views.WeatherView;

/**
 * Created by arjunpola on 5/26/17.
 */

public interface WeatherPresenter extends MvpPresenter<WeatherView> {

    void fetchCurrentWeather();

    void fetchStdDeviationForNext5Days();
}
