package challenge.twitter.com.twitterchallenge.network;

import challenge.twitter.com.twitterchallenge.mvp.model.WeatherData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by arjunpola on 5/26/17.
 */

public interface WeatherAPI {

    @GET("{weather_param}")
    Call<WeatherData> fetchWeather(@Path("weather_param") String weatherParam);
}
