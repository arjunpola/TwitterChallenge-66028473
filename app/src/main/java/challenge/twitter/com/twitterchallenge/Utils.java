package challenge.twitter.com.twitterchallenge;

import java.util.List;

/**
 * Created by arjunpola on 5/26/17.
 */

public class Utils {

    public static double getAverage(List<Double> data) {

        if (data == null)
            return 0f;

        float sum = 0f;

        for (int i = 0; i < data.size(); i++) {
            sum += data.get(i);
        }

        return sum / data.size();
    }

    public static double getSquaredSum(List<Double> data, double avg) {

        if (data == null)
            return 0f;

        double sqSum = 0;
        for (int i = 0; i < data.size(); i++) {
            double d = data.get(i) - avg;
            sqSum += Math.pow(d, 2);
        }

        return sqSum;
    }

    public static double getStdDev(List<Double> data) {

        if (data == null)
            return 0f;

        double average = getAverage(data);
        double squaredSum = getSquaredSum(data, average);
        double std = Math.sqrt(squaredSum / (data.size() - 1));
        return Math.round(std * 100.0) / 100.0;
    }

    public static double getTempInFah(double cel) {
        double fah = (cel * 9 / 5.0) + 32;
        return Math.round(fah * 100.0) / 100.0;
    }
}
