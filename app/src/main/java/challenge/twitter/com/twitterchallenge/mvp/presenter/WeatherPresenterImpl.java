package challenge.twitter.com.twitterchallenge.mvp.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import challenge.twitter.com.twitterchallenge.Utils;
import challenge.twitter.com.twitterchallenge.mvp.model.WeatherData;
import challenge.twitter.com.twitterchallenge.mvp.model.WeatherModelImpl;
import challenge.twitter.com.twitterchallenge.mvp.views.WeatherView;

/**
 * Created by arjunpola on 5/26/17.
 */

public class WeatherPresenterImpl extends MvpBasePresenter<WeatherView> implements WeatherPresenter {

    private WeatherModelImpl model;

    public WeatherPresenterImpl() {
        model = new WeatherModelImpl();
    }

    @Override
    public void attachView(WeatherView view) {
        super.attachView(view);
        EventBus.getDefault().register(this);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void fetchCurrentWeather() {
        model.getCurrentWeather();
    }

    @Override
    public void fetchStdDeviationForNext5Days() {
        model.getWeatherForNext5Days();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void didFetchCurrentWeatherInfo(WeatherModelImpl.WeatherInfoEvent weatherInfo) {
        WeatherView weatherView = getView();
        if (weatherView != null) {
            weatherView.showCurrentWeatherInfo(weatherInfo.getWeatherData());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void didFetchStdWeatherInfo(WeatherModelImpl.WeatherInfoListEvent weatherInfoListEvent) {
        WeatherView weatherView = getView();
        List<Double> temp = new ArrayList<>();
        List<WeatherData> weatherDataList = weatherInfoListEvent.getWeatherDataList();
        if (weatherDataList != null) {

            for (int i = 0; i < weatherDataList.size(); i++) {
                temp.add(weatherDataList.get(i).weatherInfo.temp);
            }

            Double stdCel = Utils.getStdDev(temp);
            Double stdFah = Utils.getTempInFah(stdCel);

            if (weatherView != null) {
                weatherView.showStdDeviation(String.valueOf(stdCel), String.valueOf(stdFah));
            }
        }
    }
}
