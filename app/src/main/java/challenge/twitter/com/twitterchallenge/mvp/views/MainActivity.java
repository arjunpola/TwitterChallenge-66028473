package challenge.twitter.com.twitterchallenge.mvp.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import challenge.twitter.com.twitterchallenge.R;
import challenge.twitter.com.twitterchallenge.Utils;
import challenge.twitter.com.twitterchallenge.mvp.model.WeatherData;
import challenge.twitter.com.twitterchallenge.mvp.presenter.WeatherPresenter;
import challenge.twitter.com.twitterchallenge.mvp.presenter.WeatherPresenterImpl;

public class MainActivity extends MvpActivity<WeatherView, WeatherPresenter> implements WeatherView {

    public static final String TEMP_CEL = "TEMP_CEL";
    public static final String TEMP_FAH = "TEMP_FAH";
    public static final String NAME = "NAME";
    public static final String WIND_SPEED = "WIND_SPEED";
    public static final String STD_TEMP_CEL = "STD_TEMP_CELL";
    public static final String STD_TEMP_FAH = "STD_TEMP_FAH";
    public static final String IMAGE_VISIBILE = "IMAGE_VISIBLE";

    private TextView tempCel;
    private TextView tempFah;
    private TextView name;
    private TextView windSpeed;
    private TextView stdTempCel;
    private TextView stdTempFah;
    private ImageView imageView;

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tempCel = (TextView) findViewById(R.id.temp_cel);
        tempFah = (TextView) findViewById(R.id.temp_fah);
        name = (TextView) findViewById(R.id.name);
        windSpeed = (TextView) findViewById(R.id.wind_speed);
        stdTempCel = (TextView) findViewById(R.id.std_temp_cel);
        stdTempFah = (TextView) findViewById(R.id.std_temp_fah);
        imageView = (ImageView) findViewById(R.id.cloud);
        button = (Button) findViewById(R.id.fetch_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.fetchStdDeviationForNext5Days();
            }
        });

        if (savedInstanceState == null) {
            presenter.fetchCurrentWeather();
        } else {
            tempCel.setText(savedInstanceState.getString(TEMP_CEL));
            tempFah.setText(savedInstanceState.getString(TEMP_FAH));
            name.setText(savedInstanceState.getString(NAME));
            windSpeed.setText(savedInstanceState.getString(WIND_SPEED));
            stdTempCel.setText(savedInstanceState.getString(STD_TEMP_CEL));
            stdTempFah.setText(savedInstanceState.getString(STD_TEMP_FAH));
            imageView.setVisibility(savedInstanceState.getInt(IMAGE_VISIBILE));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString(TEMP_CEL, tempCel.getText().toString());
        savedInstanceState.putString(TEMP_FAH, tempFah.getText().toString());
        savedInstanceState.putString(NAME, name.getText().toString());
        savedInstanceState.putString(WIND_SPEED, windSpeed.getText().toString());
        savedInstanceState.putString(STD_TEMP_CEL, stdTempCel.getText().toString());
        savedInstanceState.putString(STD_TEMP_FAH, stdTempFah.getText().toString());
        savedInstanceState.putInt(IMAGE_VISIBILE, imageView.getVisibility());
    }

    @Override
    public void showCurrentWeatherInfo(WeatherData weatherData) {
        tempCel.setText(stringOf(weatherData.weatherInfo.temp));
        tempFah.setText(stringOf(Utils.getTempInFah(weatherData.weatherInfo.temp)));
        name.setText(weatherData.name);
        windSpeed.setText(stringOf(weatherData.windInfo.speed));
        if (Double.compare(weatherData.windInfo.speed, 0.5) > 0) {
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showStdDeviation(String cel, String fah) {
        stdTempCel.setText(cel);
        stdTempFah.setText(fah);
    }

    @NonNull
    @Override
    public WeatherPresenter createPresenter() {
        return new WeatherPresenterImpl();
    }

    String stringOf(double val) {
        return String.valueOf(val);
    }
}
