package challenge.twitter.com.twitterchallenge.mvp.model;

/**
 * Created by arjunpola on 5/26/17.
 */

public interface WeatherModel {

    void getCurrentWeather();

    void getWeatherForNext5Days();
}
