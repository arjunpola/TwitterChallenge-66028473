package challenge.twitter.com.twitterchallenge.mvp.model;

/**
 * Created by arjunpola on 5/26/17.
 */

import com.google.gson.annotations.SerializedName;

public class WeatherData {

    public String name;

    @SerializedName("weather")
    public WeatherInfo weatherInfo;

    @SerializedName("wind")
    public WindInfo windInfo;

    @SerializedName("clouds")
    public CloudsInfo cloudsInfo;

    public static class WeatherInfo {

        public double temp;

        public int pressure;

        public int humidity;
    }

    public static class WindInfo {
        public double speed;

        public int deg;
    }

    public static class CloudsInfo {
        public int cloudiness;
    }

}
