package challenge.twitter.com.twitterchallenge;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arjunpola on 5/26/17.
 */

public class UtilsTest {

    @Test
    public void getsCorrectAverageOfDoubleValues() throws Exception {

        List<Double> vals = new ArrayList<>();
        vals.add(2d);
        vals.add(4d);

        double avg = Utils.getAverage(vals);

        Assert.assertEquals(3d, avg, 0);
    }

    @Test
    public void getsCorrectSQStandardDeviation() throws Exception {

        List<Double> vals = new ArrayList<>();
        vals.add(2d);
        vals.add(4d);
        vals.add(6d);
        vals.add(8d);
        vals.add(10d);

        double avg = Utils.getAverage(vals);
        double sqStd = Utils.getSquaredSum(vals, avg);

        Assert.assertEquals(40, sqStd, 0);
    }

    @Test
    public void getsCorrectStandardDeviation() throws Exception {

        List<Double> vals = new ArrayList<>();
        vals.add(2d);
        vals.add(4d);
        vals.add(6d);
        vals.add(8d);
        vals.add(10d);

        double std = Utils.getStdDev(vals);

        Assert.assertEquals(3.16d, std, 0);
    }

}
