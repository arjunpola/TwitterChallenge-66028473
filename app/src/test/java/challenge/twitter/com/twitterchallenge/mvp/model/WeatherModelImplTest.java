package challenge.twitter.com.twitterchallenge.mvp.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import retrofit2.Call;

/**
 * Created by arjunpola on 5/26/17.
 */

public class WeatherModelImplTest {

    WeatherModelImpl weatherModel;

    @Before
    public void setup() {
        weatherModel = new WeatherModelImpl();
    }

    @Test
    public void fetchesCorrectWeatherParameter() throws Exception {
        //GIVEN
        int day = 2;

        //WHEN
        String weatherParam = weatherModel.getWeatherParam(day);

        //THEN
        Assert.assertEquals("future_2.json", weatherParam);
    }

    @Test
    public void fetchesCurrentWeatherParameter() throws Exception {
        //GIVEN
        int day = 0;

        //WHEN
        String weatherParam = weatherModel.getWeatherParam(day);

        //THEN
        Assert.assertEquals("current.json", weatherParam);
    }
}
